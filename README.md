# Bolt Standalone

## Prerequisite

* Install Docker from [here](https://www.docker.com)

## Running


## Gauge Container
Develop you testing in the Gauge folder, setup environmnet and build.

To build/run the gauge container `bolt` itself.

```bash
docker-compose build bolt
```

Run the following to run gauge tests from maven. The entry point command for `sut` container is `mvn`.

```bash

Tests can be selected with the following command us "," for multiple tests or select entire folder of specs.
Env to run on is select with -Denv=qa

docker run -d --name bolt10 bolt1120_bolt gauge:execute -DspecsDir=specs/JmeterTest.spec

```
The bolt1120 will be the name of you project folder
Bolt10 - name can be anything to name the container.

The above is equivalent to running `mvn test`

