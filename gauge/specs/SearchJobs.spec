SearchPage
===========

     |jobFunction   |location       |keyword|results|
     |--------------|---------------|-------|-------|
     |Office        |Atlanta, GA    |Trial1 |0      |
     |DevOps        |Portland, OR   |Trial2 |0      |


* Go to swat solutions website
* Navigate to "WORK HERE" tab
* Click "View Open Positions" button

Search For Jobs
---------------
* Search openings <jobFunction> <location> <keyword>
* Verify <results> results

This is a sample of another way to run a test
Sample Second Run
-----------------
* Search openings "" "" ""
* Verify "8" results

Sample Negative Test
--------------------
* Verify value "BEN" doesn't exist in dropdown "1"
* Verify value "BEN" doesn't exist in dropdown "2"